from dateutil import parser
import pandas as pd
import numpy as np
import re
import locale

def DateChecker(date):
    try:
        a = parser.parse(date).date()
        return True
    except:
        return False


def dateDropper(df):
    dateRegexList = ['^([1-9]|([012][0-9])|(3[01]))-([0]{0,1}[1-9]|1[012])-\d\d\d\d [012]{0,1}[0-9]:[0-6][0-9]$', \
                     '20\d{2}(-|\/)((0[1-9])|(1[0-2]))(-|\/)((0[1-9])|([1-2][0-9])|(3[0-1]))(\s)(([0-1][0-9])|(2[0-3])):([0-5][0-9]):([0-5][0-9])', \
                     '^([1-9]|([012][0-9])|(3[01]))-([0]{0,1}[1-9]|1[012])-\d\d\d\d (20|21|22|23|[0-1]?\d):[0-5]?\d:[0-5]?\d$', \
                     '/(?:(\d{4})([\-\/.])([0-3]?\d)\2([0-3]?\d)|([0-3]?\d)([\-\/.])([0-3]?\d)\6(\d{4}))(?:\s+([012]?\d)([:hap])([0-5]\d))?/', \
                     '\d{4}-\d{2}-\d{2}\s+\d{2}:\d{2}:\d{2}.0*', \
                     "/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]) (0[0-9]|1[0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/"]
    for col in df.columns:
        try:
            dateCandidate = df[col][df[col].notnull()].sample().values[0]
        except:
            dateCandidate = ''

        if type(dateCandidate) == 'datetime.datetime' or type(dateCandidate) == np.datetime64 or DateChecker(
                dateCandidate):
            df = df.drop([col], axis=1)
        else:
            for dateR in dateRegexList:
                if re.match(dateR, str(dateCandidate)):
                    df = df.drop([col], axis=1)

    return df


def columnAnalyser(df):
    numCols, textCols = columnSplitter(df)
    df = df.dropna(axis=1, how='all')
    analyzedDf = {}
    colNames = []
    uniquePercent = []
    nanPercent = []
    zeroPercent = []
    isAlfaNumeric = []
    averageLen = []
    ContainsNum = []
    standartDeviation = []
    lenDeviation=[]
    Mean = []
    for col in df.columns:
        col_len = len(df[col].tolist())
        col_sample = df[col][df[col].notnull()].sample().values[0]
        colNames.append(col)
        uniquePercent.append(len(df[col].unique().tolist()) / col_len)
        nanPercent.append(df[col].isna().sum() / col_len)
        isAlfaNumeric.append(((str(df[col][df[col].notnull()].sample().values[0])).replace(' ', "")).isalnum())
        averageLen.append(len(str((df[col][df[col].notnull()].sample().values[0])).replace(' ', "")))
        zeroPercent.append((col_len - df[col].astype(bool).sum(axis=0)) / col_len)
        ContainsNum.append(any(str(i).isdigit() for i in str(col_sample)))
        lenDeviation.append(df[col][df[col].notnull()].apply(lambda x : len(x)).std())
        if len(str(col_sample)) >= 10 and str(col_sample).isnumeric():
            standartDeviation.append('asStr')
            Mean.append('asStr')
        else:
            try:
                standartDeviation.append(df[col].std())

            except:
                standartDeviation.append('Nan')

            try:
                Mean.append(df[col].mean())
            except:
                Mean.append('Nan')
        return pd.DataFrame(analyzedDf)  # Add Column Alphabet state


def columnSplitter(df):
    numericCol = []
    textRow = []
    for col in df.columns:
        colFlag = True
        nanRow = df[col].sample().values[0]
        splittedRow = str(nanRow).split(' ')
        for item in splittedRow:
            if len(item) < 10 and TryCastToFloat(item):
                colFlag = False
            elif intCheckHelper(item) and len(item) < 10:
                colFlag = False
        if colFlag:
            textRow.append(col)
        else:
            numericCol.append(col)
    for col in textRow:
        try:
            df[col] = df[col].apply(lambda x: x.strip())
        except:
            pass
    return numericCol, textRow



def TryCastToFloat(x):

    try:

        locale.setlocale(locale.LC_ALL, 'Turkish_Turkey.1254')
    except locale.Error as e:
        locale.setlocale(locale.LC_ALL, 'C')
        print('localization failed,export your LC_ALL TO Turkish_Turkey.1254', e)
    try:
        float(x)
        return True
    except:
        if (type(x) is str) and (x.find('.') != -1 or x.find('.T') != -1):
            x = x.replace('.T', '.0')
            x = x.replace('.', ',')
        elif type(x) in (int, float, np.float, np.int):
            return True
        try:
            locale.atof(x)
            return True
        except:
            try:
                x = x.replace(',', '.', 1)
                locale.atof(x)
                return True
            except:
                return False


def file_reader(path):
    import pandas as pd
    file = pd.DataFrame({'EmptyFrame': [0]})
    try:
        file = pd.read_csv(path, sep='\s*\*?\,?', engine='python', names='')
        file.columns = [str("Col" + str(i)) for i in range(len(file.columns))]
        file = file.dropna(axis=1, how='all')
        file = file.dropna(axis=0, how='all')
        print("File're reading with regex='\s*\*'...")
        print('Succeed,Done')
    except:
        print("File Cannot read,other types're being tried..")
        try:
            file = pd.read_excel(path)
            file.columns = [str("Col" + str(i)) for i in range(len(file.columns))]
            file = file.dropna(axis=1, how='all')
            file = file.dropna(axis=0, how='all')
            print('Succeed,Done')

        except:
            print('file cannot read')
    file = dateDropper(file)
    return file


# Methonds on pandas DataFrame row
def numericSummer(df2):
    df=df2.copy()
    numeric_col, _ = columnSplitter(df)
    for col in numeric_col:
        df[col] = df[col].apply(lambda x: rowSummerHelper(x))
    return df


def rowSummerHelper(item):
    sums = 0
    if type(item) == int or type(item) == float:
        sums = item
    else:
        inner_items = item.split(' ')
        for i in inner_items:
            if i.find('.') != -1:
                i = i.replace(',', '')
                sums += float(i)
    return sums


def intCheckHelper(s):
    try:
        int(s)
        return True
    except:
        return False


def Vektorizer(df):
    vektor = df.copy()
    vektor = vektor.drop('Column Name', axis=1)
    vektor = vektor.drop('Mean', axis=1)
    vektor = vektor.drop('standartDeviation', axis=1)
    vektor['isAlfaNumeric'] = vektor['isAlfaNumeric'].apply(lambda x: 1 if x == True else 0)
    vektor['ContainsNum'] = vektor['ContainsNum'].apply(lambda x: 1 if x == True else 0)
    vektor['averageLen'] = vektor['averageLen'] * 0.1

    vektor['Column'] = df['Column Name']
    # weight to col
    return vektor


from scipy.spatial import distance


def ColumnMatcher(leftDf, rightDf):
    pairs = {'leftCol': [], 'rightCol': [], 'distance': []}
    leftCols = []
    rightCols = []
    distances = []
    leftAnalyzed = columnAnalyser(leftDf)
    leftVektors = Vektorizer(leftAnalyzed)
    rightAnalayzed = columnAnalyser(rightDf)
    rightVektors = Vektorizer(rightAnalayzed)
    for (leftColName, leftVektor) in zip(leftVektors['Column'].values, leftVektors.drop('Column', axis=1).values):
        for (rightColName, rightVektor) in zip(rightVektors['Column'].values,
                                               rightVektors.drop('Column', axis=1).values):
            leftCols.append(leftColName)
            rightCols.append(rightColName)
            distances.append(distance.euclidean(leftVektor, rightVektor))
    pairs['leftCol'] = leftCols
    pairs['rightCol'] = rightCols
    pairs['distance'] = distances
    df = pd.DataFrame(pairs)
    matchOnTicketNumber = df.sort_values(['leftCol', 'distance']).values[0]
    matchOnPnr = df[df['leftCol'] == 'Col2'].sort_values(['distance']).values[0]
    matchOnTotal = df[df['leftCol'] == 'Col21'].sort_values('distance').values[0]
    return matchOnTicketNumber, matchOnPnr, matchOnTotal, df  # evaluate column inner join before merge.


def RowMatcher(leftDf, rightDf):
    ticketMatch, pnrMatch, totalMatch, df = ColumnMatcher(leftDf, rightDf)
    LeftTicketCol, RightTicketCol = ticketMatch[0], ticketMatch[1]
    LeftTPnrCol, RightPnrCol = pnrMatch[0], pnrMatch[1]
    mergeOnTicketNo = RowMatch(leftDf, rightDf, LeftTicketCol, RightTicketCol)
    mergeOnPnrNo = RowMatch(leftDf, rightDf, LeftTPnrCol, RightPnrCol)
    return mergeOnTicketNo, mergeOnPnrNo


def RowMatch(left, right, leftCol, rightCol):
    try:
        left[leftCol] = left[leftCol].apply(lambda x: str(int(x)))
        right[rightCol] = right[rightCol].apply(lambda x: str(int(x)))
    except:
        left[leftCol] = left[leftCol].apply(lambda x: str(x))
        right[rightCol] = right[rightCol].apply(lambda x: str(x))

    rightColLen = int(round(right[rightCol].apply(lambda x: len(x)).mean()))
    leftColLen = int(round(left[leftCol].apply(lambda x: len(x)).mean()))
    print(rightColLen, leftColLen)

    if (rightColLen != leftColLen):
        minCol = min(rightColLen, leftColLen)
        right[rightCol] = right[rightCol].apply(lambda x: x[-minCol:])
        left[leftCol] = left[leftCol].apply(lambda x: x[-minCol:])

    leftGroup = left.groupby(leftCol).agg(lambda x: list(x))
    rightGroup = right.groupby(rightCol).agg(lambda x: list(x))
    outerMergeOnPnr = leftGroup.merge(rightGroup, left_index=True, right_index=True, how='outer', indicator='isOuter')

    return outerMergeOnPnr


def myAgg(series):
    from functools import reduce
    sample=series.sample(1).values[0]
    if TryCastToFloat(sample):
        return( reduce(lambda x,y: int(x)+int(y),series))
    else:
        return set(series)
    analyzedDf['Column Name'] = colNames
    analyzedDf['UniquePercent'] = [x * 10 for x in uniquePercent]
    analyzedDf['NanPercent'] = nanPercent
    analyzedDf['zeroPercent'] = zeroPercent
    analyzedDf['isAlfaNumeric'] = isAlfaNumeric
    analyzedDf['averageLen'] = [x * 5 for x in averageLen]
    analyzedDf['lenDeviation']=lenDeviation
    analyzedDf['ContainsNum'] = ContainsNum
    analyzedDf['Mean'] = Mean
    analyzedDf['standartDeviation'] = standartDeviation

