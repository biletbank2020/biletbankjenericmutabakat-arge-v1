from Template import ITemplate
from File import  Matcher,Result
import pickle
from copy import copy



class Action:
    def __init__(self, left: ITemplate, right: ITemplate):
        self.left = left
        self.right=right
        self.function = sum
        self.serialized =False
        self.actionName=left.behavior.name+'-'+right.behavior.name

    def build(self):
        a=copy(self.left)
        b=copy(self.right)

        mtchr = Matcher( a, b)
        #mtchr.fitAndEstimate()
        logicalCols=self.logicalColmDecider()
    def logicalColmDecider(self):
        analyzedLeftCol=self.left.behavior.analyzed
        analyzedLeftCol=analyzedLeftCol[analyzedLeftCol['Column Name'].isin(self.left.behavior.textCols)].query('UniquePercent<0.1')
        analyzedRightCol=self.right.behavior.analyzed
    def serialize(self):
        if self.serialized == True:
            pass
        else:
            self.left.behavior.leanFrame=None
            self.right.behavior.leanFrame=None
            self.serialized==True
            tlist = ActionList()
            tlist.Load()
            tlist.actionList.append(self)
            tlist.Save()

class ActionList:
    path = 'actionList.txt'
    def __init__(self):
        self.actionList=[]
    def Save(self):
        ActionListSaver(self)
    def Load(self):
        self.actionList=ActionListLoader()
def ActionListSaver(list:ActionList):
        with open(ActionList.path, 'wb') as outfile:
            pickle.dump(list.__dict__, outfile)
            print('Action"ve been read')
def ActionListLoader():
    try:
        with open(ActionList.path,'rb') as file :
            dict= pickle.load(file)
            print('Actions"ve been read')
    except:
        return []
    t_list=ActionList()
    t_list.__dict__=dict
    return t_list.actionList