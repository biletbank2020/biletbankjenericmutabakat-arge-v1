from dateutil import parser
import pandas as pd
import numpy as np
import re
import locale
from HelperFunctions import columnAnalyser, Vektorizer


class File():
    fileNumber = 0

    def __init__(self, path):
        import os
        self.fileMatchId = ''
        self.base = pd.DataFrame({'EmptyFrame': [0]})
        self.originColIndex = 0
        self.columns = []
        self.numericCols = []
        self.textCols = []
        self.lenght = 0
        self.colLen = 0
        self.totalCol = 0
        self.pnrCol = ''
        self.TicketCol = ''
        self.nameCol = ''
        self.dateCol = ''
        self.name = (os.path.basename(path))
        self.path = path
        self.leanFrame = ''
        self.read()

    # implement later-> def __add__():
    # implement later->def __sub__():
    @property
    def analayzedForm(self):
        df = self.leanFrame.copy()
        df = df.dropna(axis=1, how='all')
        analyzedDf = {}
        colNames = []
        uniquePercent = []
        nanPercent = []
        zeroPercent = []
        isAlfaNumeric = []
        averageLen = []
        ContainsNum = []
        standartDeviation = []
        lenDeviation = []
        Mean = []
        for col in df.columns:
            col_len = len(df[col].tolist())
            col_sample = df[col][df[col].notnull()].sample().values[0]
            colNames.append(col)
            uniquePercent.append(len(df[col].unique().tolist()) / col_len)
            nanPercent.append(df[col].isna().sum() / col_len)
            isAlfaNumeric.append(((str(df[col][df[col].notnull()].sample().values[0])).replace(' ', "")).isalnum())
            averageLen.append(len(str((df[col][df[col].notnull()].sample().values[0])).replace(' ', "")))
            lenDeviation.append(df[col][df[col].notnull()].apply(lambda x: len(str(x))).std())

            zeroPercent.append((col_len - df[col].astype(bool).sum(axis=0)) / col_len)
            ContainsNum.append(any(str(i).isdigit() for i in str(col_sample)))
            if len(str(col_sample)) >= 10 and str(col_sample).isnumeric():
                standartDeviation.append('asStr')
                Mean.append('Nan')
            else:
                try:
                    standartDeviation.append(df[col].std())

                except:
                    standartDeviation.append('Nan')

                try:
                    Mean.append(df[col].mean())
                except:
                    Mean.append('Nan')
        analyzedDf['Column Name'] = colNames
        analyzedDf['UniquePercent'] = uniquePercent
        analyzedDf['NanPercent'] = nanPercent
        analyzedDf['zeroPercent'] = zeroPercent
        analyzedDf['isAlfaNumeric'] = isAlfaNumeric
        analyzedDf['averageLen'] = averageLen
        analyzedDf['ContainsNum'] = ContainsNum
        analyzedDf['Mean'] = Mean
        analyzedDf['lenDeviation'] = lenDeviation
        analyzedDf['standartDeviation'] = standartDeviation
        return pd.DataFrame(analyzedDf)  # Add Column Alphabet state

    @property
    def numericTable(self):
        return self.leanFrame.drop(self.textCols, axis=1)

    @property
    def textTable(self):
        return self.leanFrame.drop(self.numericCols, axis=1)

    def read(self):
        file_sample = self.name
        try:
            self.base = pd.read_csv(self.path, sep='\s*\*?\,?', engine='python', names='')
            self.columns = [str(file_sample + "_Col" + str(i)) for i in range(len(self.base.columns))]
            print("File're reading with regex='\s*\*'...")
            print('Succeed,Done')
        except pd.errors.ParserError as e:
            print(e)
            print("File Cannot read,other types're being tried..")
            try:
                self.base = pd.read_excel(self.path)
                self.columns = [file_sample + str("_Col" + str(i)) for i in range(len(self.base.columns))]
                print('Succeed,Done')
            except:
                print('file cannot read')
        self.lenght = len(self.base)
        self.base = self.base.dropna(axis=1, thresh=int(len(self.base) * 0.8), how='all')
        self.base = self.base.dropna(axis=0, thresh=int(len(self.base.columns) * 0.8), how='all')
        self.FileCropper()
        self.dateDropper()
        self.columnSplitter()
        self.detectTotalCol()

    def detectTotalCol(self):
        analyzCopy = self.analayzedForm.copy()
        analyzCopy = analyzCopy[analyzCopy['Column Name'].apply(lambda x: x in (self.numericCols))]
        maxMean = analyzCopy.query('lenDeviation!=0 and standartDeviation!=0 and zeroPercent<0.5')['Mean'].max()
        SparedmaxMean = analyzCopy.query('standartDeviation!=0 and zeroPercent<0.5')['Mean'].max()
        try:
            self.totalCol = analyzCopy['Column Name'][analyzCopy.Mean == maxMean].values[0]
        except:
            self.totalCol = analyzCopy['Column Name'][analyzCopy.Mean == SparedmaxMean].values[0]

    def FileCropper(self):
        file_sample = self.name
        candidateRow = []  # array for canddiate rows to drop
        indexes = list(self.base.index)
        headIndex = indexes[0]
        # get index for travel on data rows.
        if self.lenght < 20:  # if dataframe lower than 20 pay attantion all else first 10 and last 10 rows.
            rowIndexes = indexes
        else:
            rowIndexes = indexes[:10] + indexes[-10:]

        for row_index in rowIndexes:

            row = self.base.loc[row_index]
            nanNumberInRow = len(row[row.isna()])
            nanCastableNumberInRow = sum([1 if not self.TryCastToFloat(col) else 0 for col in row.values])
            isIncludeKeyword = any([col in ['Toplam', 'Total'] for col in row])

            if nanNumberInRow > self.colLen * .45 or (nanCastableNumberInRow == self.colLen) or isIncludeKeyword:
                candidateRow.append(row_index)
            if nanCastableNumberInRow == len(row):
                self.originColIndex = row_index

        self.leanFrame = self.base.drop(candidateRow)
        self.leanFrame.columns = [file_sample + str("_Col" + str(i)) for i in range(len(self.leanFrame.columns))]

    def TryCastToFloat(self, x):

        try:

            locale.setlocale(locale.LC_ALL, 'Turkish_Turkey.1254')
        except locale.Error as e:
            locale.setlocale(locale.LC_ALL, 'C')
            print('localization failed,export your LC_ALL TO Turkish_Turkey.1254', e)
        try:
            float(x)
            return True
        except:
            if (type(x) is str) and (x.find('.') != -1 or x.find('.T') != -1):
                x = x.replace('.T', '.0')
                x = x.replace('.', ',')
            elif type(x) in (int, float, np.float, np.int):
                return True
            try:
                locale.atof(x)
                return True
            except:
                try:
                    x = x.replace(',', '.', 1)
                    locale.atof(x)
                    return True
                except:
                    return False

    def DateChecker(self, date):
        try:
            a = parser.parse(date).date()
            return True
        except:
            return False

    def dateDropper(self):

        dateRegexList = ['^([1-9]|([012][0-9])|(3[01]))-([0]{0,1}[1-9]|1[012])-\d\d\d\d [012]{0,1}[0-9]:[0-6][0-9]$', \
                         '20\d{2}(-|\/)((0[1-9])|(1[0-2]))(-|\/)((0[1-9])|([1-2][0-9])|(3[0-1]))(\s)(([0-1][0-9])|(2[0-3])):([0-5][0-9]):([0-5][0-9])', \
                         '^([1-9]|([012][0-9])|(3[01]))-([0]{0,1}[1-9]|1[012])-\d\d\d\d (20|21|22|23|[0-1]?\d):[0-5]?\d:[0-5]?\d$', \
                         '/(?:(\d{4})([\-\/.])([0-3]?\d)\2([0-3]?\d)|([0-3]?\d)([\-\/.])([0-3]?\d)\6(\d{4}))(?:\s+([012]?\d)([:hap])([0-5]\d))?/', \
                         '\d{4}-\d{2}-\d{2}\s+\d{2}:\d{2}:\d{2}.0*', \
                         "/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]) (0[0-9]|1[0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/"]
        for col in self.columns:
            try:
                dateCandidate = self.leanFrame[col][self.leanFrame[col].notnull()].sample().values[0]
            except:
                dateCandidate = ''

            if type(dateCandidate) == 'datetime.datetime' or type(dateCandidate) == np.datetime64 or self.DateChecker(
                    dateCandidate):
                self.leanFrame = self.leanFrame.drop([col], axis=1)
            else:
                for dateR in dateRegexList:
                    if re.match(dateR, str(dateCandidate)):
                        self.leanFrame = self.leanFrame.drop([col], axis=1)

    def columnSplitter(self):
        numericCol = []
        textRow = []
        for col in self.leanFrame.columns:
            colFlag = True
            nanRow = self.leanFrame[col].sample().values[0]
            splittedRow = str(nanRow).strip().split(' ')
            for item in splittedRow:
                if (len(item) < 10 and self.TryCastToFloat(item) and len(splittedRow) <= 2):
                    colFlag = False
            if colFlag:
                textRow.append(col)
            else:
                numericCol.append(col)
        for col in textRow:
            try:
                self.leanFrame[col] = self.leanFrame[col].apply(lambda x: x.strip())
            except:
                pass
        for col in numericCol:
            self.leanFrame[col] = self.leanFrame[col]. \
                apply(lambda x: self.CastToFloat(x) if len(str(x).split(' ')) == 1 else sum(
                [self.CastToFloat(i) for i in str(x).split(' ')]))  # Product of Two Col value for exp. 30 INV = 30.00

        self.numericCols = numericCol
        self.textCols = textRow

    def CastToFloat(self, x):
        try:

            locale.setlocale(locale.LC_ALL, 'Turkish_Turkey.1254')
        except locale.Error as e:
            locale.setlocale(locale.LC_ALL, 'C')
            print('localization failed,export your LC_ALL TO Turkish_Turkey.1254', e)
        try:

            return float(x)
        except:
            if (type(x) is str) and (x.find('.') != -1 or x.find('.T') != -1):
                x = x.replace('.T', '.0')
                x = x.replace('.', ',')
            elif type(x) in (int, float, np.float, np.int):
                return float(x)
            try:
                return locale.atof(x)
            except:
                try:
                    x = x.replace(',', '.', 1)
                    return locale.atof(x)
                except:
                    return 0


class Matcher():
    matchPercent = 0

    def __init__(self, left: File, right: File):
        self.leftFile = left
        self.rightFile = right
        self.analayzedLeft = self.leftFile.analayzedForm
        self.analayzedRight = self.rightFile.analayzedForm
        self.leftVector = Vektorizer(self.analayzedLeft)
        self.leftVector['priority'] = self.leftVector['UniquePercent'] * 5 + (
                    5 * (1 - self.leftVector['lenDeviation'])) + (1 - self.leftVector['zeroPercent'])
        self.leftVectorCandidates = self.leftVector.sort_values('priority', ascending=False)[:3]
        self.rightVector = Vektorizer(self.analayzedRight)
        self.rightVector['priority'] = self.rightVector['UniquePercent'] * 5 + (
                    5 * (1 - self.rightVector['lenDeviation'])) + (1 - self.rightVector['zeroPercent'])
        self.rightVectorCandidates = self.rightVector.sort_values('priority', ascending=False)[:3]
        self.pairCandidates = []
        self.matchCandidate = []
        self.matchOnTicket = []
        self.matchOnPnr = []
        self.leftOut = []
        self.rightOut = []
        self.inner = []

    def fitAndEstimate(self):
        from scipy.spatial import distance
        pairs = {'leftCol': [], 'rightCol': [], 'distance': [], 'uniquer': []}
        leftCols = []
        rightCols = []
        distances = []
        uniquer = []
        for (leftColName, leftVector) in zip(self.leftVectorCandidates['Column'].values,
                                             self.leftVectorCandidates.iloc[:, :-2].values):
            for (rightColName, rightVector) in zip(self.rightVectorCandidates['Column'].values,
                                                   self.leftVectorCandidates.iloc[:, :-2].values):
                leftCols.append(leftColName)
                rightCols.append(rightColName)
                distances.append(distance.euclidean(leftVector, rightVector))
                uniquer.append(leftVector[0] + rightVector[0])
        pairs['leftCol'] = leftCols
        pairs['rightCol'] = rightCols
        pairs['distance'] = distances
        pairs['uniquer'] = uniquer
        df = pd.DataFrame(pairs)
        self.pairCandidates = df.sort_values('distance').iloc[:, :2].values

        for leftCol, rightCol in self.pairCandidates:

            res= self.RowMatch(self.leftFile.leanFrame, self.rightFile.leanFrame, leftCol, rightCol)
            if res is not None:
                self.matchCandidate.append


        return pd.DataFrame(self.matchCandidate)


    def RowMatch(self, left1, right1, leftCol, rightCol):
        left = left1.copy()
        right = right1.copy()
        try:
            left[leftCol] = left[leftCol].apply(lambda x: str(int(x)))
            right[rightCol] = right[rightCol].apply(lambda x: str(int(x)))
        except:
            left[leftCol] = left[leftCol].apply(lambda x: str(x))
            right[rightCol] = right[rightCol].apply(lambda x: str(x))

        rightColLen = int(round(right[rightCol].apply(lambda x: len(x)).mean()))
        leftColLen = int(round(left[leftCol].apply(lambda x: len(x)).mean()))
        print()

        if (rightColLen != leftColLen):
            minCol = min(rightColLen, leftColLen)
            right[rightCol] = right[rightCol].apply(lambda x: x[-minCol:])
            left[leftCol] = left[leftCol].apply(lambda x: x[-minCol:])

        leftGroup = left.groupby(leftCol).agg(self.myAgg)

        rightGroup = right.groupby(rightCol).agg(self.myAgg)
        leftIndexValues = left[leftCol].unique()
        rightIndexValues = right[rightCol].unique()

        for leftValue in leftIndexValues:
            for rightValue in rightIndexValues:
                if rightValue == leftValue:
                    return leftGroup.merge(rightGroup, left_index=True, right_index=True, how='outer',
                                           indicator='isOuter')

    def myAgg(self, col):
        if (col.name in file1.textCols + file2.textCols):
            return set(col)
        else:
            return sum(col)


if __name__ == '__main__':
    file1 = File("reports/allin/_459/amaWeb")
    file2 = File("reports/allin/_459/biletWeb.xlsx")
    matcher = Matcher(file1, file2)
    matched = matcher.fitAndEstimate()









