import locale
import numpy as np
import json
import pandas as pd
import pickle

class ITemplate():
    def __init__(self):
        self.colSize = 0
        self.colNames = ''
        self.colCodes = ''
        self.name=''
        self.leanFrame=''
class Action:
    def __init__(self):
        left:Template=None
        right:Template=None

        logicalActionOnLeft=lambda x : x*0#x TotalCol
        lineerActionOnLeft=sum

        logicalActionOnRight=lambda x : x*0 #x TotalCol
        lineerActionOnRight=sum#sum lineer depended col for Total Col
class Template(ITemplate):
    class Behavior:
        def __init__(self, uniquestIdentifier, identifier, totalCol):
            self.uniquestIdentifier = uniquestIdentifier
            self.identifier = identifier
            self.totalCol = totalCol
            self.leanFrame = None
            self.analyzed = None
            self.name=''
            self.textCols = ''
            self.numericCols = ''
            self.logicalDependencies = {'columnName':None,'colVal':None}
            self.lineerDependencies = []

    def __init__(self, colSize: int, colNames: str, colCodes: str, behavior):
        self.colSize = colSize
        self.colNames = colNames
        self.colCodes = colCodes
        self.serialized = False
        self.behavior = behavior
        self.name=''

    def serialize(self):
        self.serialized=True

        lean_frame=self.behavior.leanFrame
        self.behavior.leanFrame=None

        tlist = TemplateList()
        tlist.Load()
        tlist.templateList.append(self)
        tlist.Save()

        self.behavior.leanFrame=lean_frame
class Templater():
    templateNumber = 0

    def preBuildTemplate(self, file: pd.DataFrame,colNames:str,sample):
        self.colNames=colNames
        self.sample = sample
        self.colSize = file.columns.size
        self.colCodes = self.ColCoder(self.sample)
        self.template = Template(self.colSize, self.colNames, self.colCodes, Template.Behavior('','',''))


        return self.template


    def TryCastToFloat(self, x):
        locale.setlocale(locale.LC_ALL, 'C')
        try:
            float(x)
            return True
        except:
            if (type(x) is str) and (x.find('.') != -1 or x.find('.T') != -1):
                x = x.replace('.T', '.0')
                x = x.replace('.', ',')
            elif type(x) in (int, float, np.float, np.int):
                return True
            try:
                locale.atof(x)
                return True
            except:
                try:
                    x = x.replace(',', '.', 1)
                    locale.atof(x)
                    return True
                except:
                    return False

    def ColCoder(self, sample):
        return ''.join(sample.dropna().values)

    def encode(self, c):
        if c in ['a', 'e', 'i', 'u', 'o']:
            return 'V'
        elif c in ['b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'x', 'z']:
            return 'C'
        else:
            return 'O'

    def TemplateSerializator(self, template: Template):
        pass

    def TemplateDeserializator(self, template: Template):
        pass
class TemplateList:
    path = 'templateList.txt'
    def __init__(self):
        self.templateList=[]
    def Save(self):
        TemplateListSaver(self)
    def Load(self):
        self.templateList=TemplateListLoader()
def TemplateListSaver(list:TemplateList):
        with open(TemplateList.path, 'wb') as outfile:
            pickle.dump(list.__dict__, outfile)
            print('template saved')
def TemplateListLoader():
    try:
        with open(TemplateList.path,'rb') as file :
            dict= pickle.load(file)
            print('template"ve been read')
    except:
        return []
    t_list=TemplateList()
    t_list.__dict__=dict
    return t_list.templateList
