from time import time

from dateutil import parser
import pandas as pd
import numpy as np
import re
import locale
import os
from Template import Templater, Template,ITemplate
class File(ITemplate):
    fileNumber = 0

    def __init__(self, path: str):
        self.fileMatchId = ''
        self.base = pd.DataFrame({'EmptyFrame': [0]})
        self.originColIndex = 0
        self.columns = []
        self.numericCols = []
        self.textCols = []
        self.lenght = 0
        self.colLen = 0
        self.totalCol = 0
        self.originalColumn=[]
        self.pnrCol = ''
        self.TicketCol = ''
        self.nameCol = ''
        self.dateCol = ''
        self.sparedTotalCol='' #some report set 2 similar column for payment one Cash_payment and Total_Fare,this hold sparedTotal for better anaysis.
        try:
            self.name = (os.path.basename(path)).split('.')[0]
        except:
            self.name = (os.path.basename(path))
        self.path = path
        self.leanFrame = ''
        self.uniquestIdentifier = ''
        self.identifier = ''
        self.sample=''
        self.dependenCol=''
        self.haveTemplate=False
        self.haveBehavior=False
        self.haveAction=False
    # implement later-> def __add__():
    # implement later->def __sub__():
    @property
    def analayzedForm(self):
        df = self.leanFrame.copy()
        df = df.dropna(axis=1, how='all')
        analyzedDf = {}
        colNames = []
        uniquePercent = []
        nanPercent = []
        zeroPercent = []
        isAlfaNumeric = []
        averageLen = []
        ContainsNum = []
        standartDeviation = []
        lenDeviation = []
        Mean = []
        for col in df.columns:
            col_len = len(df[col].tolist())
            col_sample = df[col][df[col].notnull()].sample().values[0]
            colNames.append(col)
            uniquePercent.append(len(df[col].unique().tolist()) / col_len)
            nanPercent.append(df[col].isna().sum() / col_len)
            isAlfaNumeric.append(((str(df[col][df[col].notnull()].sample().values[0])).replace(' ', "")).isalnum())
            averageLen.append(len(str((df[col][df[col].notnull()].sample().values[0])).replace(' ', "")))
            lenDeviation.append(df[col][df[col].notnull()].apply(lambda x: len(str(x))).std())

            zeroPercent.append((col_len - df[col].astype(bool).sum(axis=0)) / col_len)
            ContainsNum.append(any(str(i).isdigit() for i in str(col_sample)))
            if len(str(col_sample)) >= 9 and str(col_sample).isnumeric():
                standartDeviation.append('asStr')
                Mean.append('Nan')
            else:
                try:
                    standartDeviation.append(df[col].std())

                except:
                    standartDeviation.append('Nan')

                try:
                    Mean.append(df[col].mean())
                except:
                    Mean.append('Nan')
        analyzedDf['Column Name'] = colNames
        analyzedDf['UniquePercent'] = uniquePercent
        analyzedDf['NanPercent'] = nanPercent
        analyzedDf['zeroPercent'] = zeroPercent
        analyzedDf['isAlfaNumeric'] = isAlfaNumeric
        analyzedDf['averageLen'] = averageLen
        analyzedDf['ContainsNum'] = ContainsNum
        analyzedDf['Mean'] = Mean
        analyzedDf['lenDeviation'] = lenDeviation
        analyzedDf['standartDeviation'] = standartDeviation
        return pd.DataFrame(analyzedDf)  # Add Column Alphabet state

    @property
    def numericTable(self):
        return self.leanFrame.drop(self.textCols, axis=1)

    @property
    def textTable(self):
        return self.leanFrame.drop(self.numericCols, axis=1)

    def read(self):
        file_sample = self.name
        try:
            self.base = pd.read_csv(self.path, sep='\s*\*?\,?', engine='python', names='', dtype=str)
            self.columns = [str(file_sample + "_Col" + str(i)) for i in range(len(self.base.columns))]
            print("File're reading with regex='\s*\*'...")
            print('Succeed,Done')
        except pd.errors.ParserError as e:
            print(e)
            print("File Cannot read,other types're being tried..")
            try:
                self.base = pd.read_excel(self.path, dtype=str)
                self.columns = [file_sample + str("_Col" + str(i)) for i in range(len(self.base.columns))]
                print('Succeed,Done')
            except Exception as  e:
                print(e.args)
                print('file cannot read')
        self.lenght = len(self.base)
        self.base = self.base.dropna(axis=1, thresh=int(len(self.base) * 0.75), how='all')
        self.base = self.base.dropna(axis=0, thresh=int(len(self.base.columns) * 0.8), how='all')
        self.base.columns, self.sample = self.detectColNameAndSample(self.base)
        self.originalColumn = self.base.columns.values
        self.colLen = len(self.columns)
        self.RowDropper()
        self.DateDropper()
        self.columnSplitter()

    def build(self):
        self.detectTotalCol()
        self.detectTicketAndPnr()







    def detectColNameAndSample(self, df):
        originalColumn = df.columns.values
        indexs = df.index
        firsSampleIndex = 0
        for index in indexs:
            if len(df.loc[index]) == sum([1 if not self.TryCastToFloat(col) else 0 for col in df.loc[index]]):
                originalColumn = df.loc[index]
                firsSampleIndex = index + 1
                break
        return [self.name+'_'+i for i in originalColumn],df.loc[firsSampleIndex]
    def detectTotalCol(self):
        analyzCopy = self.analayzedForm.copy()
        analyzCopy = analyzCopy[analyzCopy['Column Name'].apply(lambda x: x in (self.numericCols))]
        maxMean = \
            analyzCopy.query('lenDeviation!=0 and standartDeviation!=0 and standartDeviation<5000 and zeroPercent<0.5 and 5<=averageLen<=13')[
                'Mean'].max()
        SparedmaxMean = analyzCopy.query('standartDeviation!=0 and zeroPercent<0.5')['Mean'].max()
        try:
            totalCols=list(analyzCopy['Column Name'][analyzCopy.Mean == maxMean])
            if len(totalCols)>1: # get last item and drop other :
                self.totalCol = totalCols.pop(0)#get last Col which is already identify total Col,if one file have to identical total_Col Drop one for lineer analyses correction.
                self.leanFrame=self.leanFrame.drop(totalCols[:-1],axis=1)#and drop others
                self.sparedTotalCol=list(totalCols).pop()
                self.numericCols.remove(self.sparedTotalCol)
            else:
                self.totalCol=list(totalCols).pop() # if candidte of total cols include only one ,assign it total col.

        except:
            self.totalCol = analyzCopy['Column Name'][analyzCopy.Mean == SparedmaxMean].values[0]

    def detectTicketAndPnr(self):
        queriedAnalayz = self.analayzedForm.query(
            'UniquePercent>0.4 and zeroPercent<0.2 and lenDeviation<1.2 and Mean=="Nan" and (6<=averageLen<7 or   10<=averageLen<=13)').sort_values(
            'averageLen', ascending=False)
        UniqueIdentifiers = queriedAnalayz['Column Name'].values
        if len(UniqueIdentifiers) < 1:
            print('No Match')
        elif len(UniqueIdentifiers) == 2:
            self.uniquestIdentifier = UniqueIdentifiers[0]
            self.identifier = UniqueIdentifiers[1]
        elif len(UniqueIdentifiers) == 1:
            self.uniquestIdentifier = UniqueIdentifiers[0]
        else:
            UniqueIdentifiers = queriedAnalayz.sort_values('UniquePercent', ascending=False)['Column Name'].values
            self.uniquestIdentifier = UniqueIdentifiers[0]
            self.identifier = UniqueIdentifiers[1]

    def RowDropper(self):
        file_sample = self.name
        candidateRow = []  # array for canddiate rows to drop
        indexes = list(self.base.index)
        headIndex = indexes[0]
        # get index for travel on data rows.
        if self.lenght < 20:  # if dataframe lower than 20 pay attantion all else first 10 and last 10 rows.
            rowIndexes = indexes
        else:
            rowIndexes = indexes[:10] + indexes[-10:]

        for row_index in rowIndexes:

            row = self.base.loc[row_index]
            nanNumberInRow = len(row[row.isna()])
            nanCastableNumberInRow = sum([1 if not self.TryCastToFloat(col) else 0 for col in row.values])
            isIncludeKeyword = any([col in ['İşlem Tarihi','Rez. No','No','Toplam', 'Total', 'Net','CHKID'] for col in row])

            if nanNumberInRow > self.colLen * .45 or (nanCastableNumberInRow == self.colLen) or isIncludeKeyword:
                candidateRow.append(row_index)
            if nanCastableNumberInRow == len(row):
                self.originColIndex = row_index
        self.leanFrame = self.base.drop(candidateRow)
        #self.leanFrame.columns = [file_sample + str("_Col" + str(i)) for i in range(len(self.leanFrame.columns))]

    def TryCastToFloat(self, x):

        locale.setlocale(locale.LC_ALL, 'C')
        try:
            float(x)
            return True
        except:
            if (type(x) is str) and (x.find('.') != -1 or x.find('.T') != -1):
                x = x.replace('.T', '.0')
                x = x.replace('.', ',')
            elif type(x) in (int, float, np.float, np.int):
                return True
            try:
                locale.atof(x)
                return True
            except:
                try:
                    x = x.replace(',', '.', 1)
                    locale.atof(x)
                    return True
                except:
                    return False

    def DateChecker(self, date):
        if self.TryCastToFloat(date):
            return False
        else:
            try:
                a = parser.parse(date).date()
                return True
            except:
                return False

    def DateDropper(self):

        dateRegexList = ['^([1-9]|([012][0-9])|(3[01]))-([0]{0,1}[1-9]|1[012])-\d\d\d\d [012]{0,1}[0-9]:[0-6][0-9]$',
                         '20\d{2}(-|\/)((0[1-9])|(1[0-2]))(-|\/)((0[1-9])|([1-2][0-9])|(3[0-1]))(\s)(([0-1][0-9])|(2[0-3])):([0-5][0-9]):([0-5][0-9])',
                         '^([1-9]|([012][0-9])|(3[01]))-([0]{0,1}[1-9]|1[012])-\d\d\d\d (20|21|22|23|[0-1]?\d):[0-5]?\d:[0-5]?\d$',
                         '/(?:(\d{4})([\-\/.])([0-3]?\d)\2([0-3]?\d)|([0-3]?\d)([\-\/.])([0-3]?\d)\6(\d{4}))(?:\s+([012]?\d)([:hap])([0-5]\d))?/',
                         '\d{4}-\d{2}-\d{2}\s+\d{2}:\d{2}:\d{2}.0*',
                         "/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]) (0[0-9]|1[0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/"]
        for col in self.leanFrame.columns:
            try:
                dateCandidate = self.leanFrame[col][self.leanFrame[col].notnull()].sample().values[0]
            except:
                dateCandidate = ''

            if type(dateCandidate) == 'datetime.datetime' or type(dateCandidate) == np.datetime64 or self.DateChecker(
                    dateCandidate):
                self.dateCol=col
                self.leanFrame = self.leanFrame.drop([col], axis=1)
                return
            else:
                for dateR in dateRegexList:
                    if re.match(dateR, str(dateCandidate)):
                        self.dateCol = col
                        self.leanFrame = self.leanFrame.drop([col], axis=1)
                        return
    def columnSplitter(self):
        numericCol = []
        textRow = []
        for col in self.leanFrame.columns:
            colFlag = True
            nanRow = self.leanFrame[col].sample().values[0]
            splittedRow = str(nanRow).strip().split(' ')
            for item in splittedRow:
                if (len(item) < 9 and self.TryCastToFloat(item) and len(splittedRow) <= 2):
                    colFlag = False
            if colFlag:
                textRow.append(col)
            else:
                numericCol.append(col)
        for col in textRow:
            try:
                self.leanFrame[col] = self.leanFrame[col].apply(lambda x: x.strip())
            except:
                pass
        for col in numericCol:
            self.leanFrame[col] = self.leanFrame[col]. \
                apply(lambda x: self.CastToFloat(x) if len(str(x).split(' ')) == 1 else sum(
                [self.CastToFloat(i) for i in str(x).split(' ')]))  # Product of Two Col value for exp. 30 INV = 30.00

        self.numericCols = numericCol
        self.textCols = textRow

    def CastToFloat(self,x):
        try:

            locale.setlocale(locale.LC_ALL, 'Turkish_Turkey.1254')
        except locale.Error as e:
            locale.setlocale(locale.LC_ALL, 'C')
            print('localization failed,export your LC_ALL TO Turkish_Turkey.1254', e)
        try:

            return float(x)
        except:
            if (type(x) is str) and (x.find('.') != -1 or x.find('.T') != -1):
                x = x.replace('.T', '.0')
                x = x.replace('.', ',')
            elif type(x) in (int, float, np.float, np.int):
                return float(x)
            try:
                return locale.atof(x)
            except:
                try:
                    x = x.replace(',', '.', 1)
                    return locale.atof(x)
                except:
                    return 0
class Matcher():
    matchPercent = 0

    def __init__(self, left: ITemplate, right: ITemplate):
        self.isTemplateLeft=isinstance(left,Template)
        self.isTemplateRight=isinstance(right,Template)
        self.left1=left.behavior if self.isTemplateLeft else left
        self.right1=right.behavior if self.isTemplateRight else right

        self.matchonUniquestIdentifier = ''
        self.matchOnIdentifier = []
        self.matchCandidate = []

    def fitAndEstimate(self):


        try:
            self.matchonUniquestIdentifier = self.RowMatch(self.left1.leanFrame, self.right1.leanFrame,
                                                       self.left1.uniquestIdentifier,
                                                       self.right1.uniquestIdentifier)
        except:
            print('No Match on Head')
        try:
            self.matchOnIdentifier = self.RowMatch(self.left1.leanFrame, self.right1.leanFrame,
                                               self.left1.identifier, self.right1.identifier)
        except:
            print('No match on Identifier')
        if (len(self.matchonUniquestIdentifier)) > 0 or len(self.matchOnIdentifier) > 0:
            print('There is a Matched Rows')
        else:
            print('no matched Row,check your files')

    def RowMatch(self, left1, right1, leftCol, rightCol):
        left = left1.copy()
        right = right1.copy()
        try:
            left[leftCol] = left[leftCol].apply(lambda x: str(int(x)))
            right[rightCol] = right[rightCol].apply(lambda x: str(int(x)))
        except:
            left[leftCol] = left[leftCol].apply(lambda x: str(x))
            right[rightCol] = right[rightCol].apply(lambda x: str(x))

        rightColLen = int(round(right[rightCol].apply(lambda x: len(x)).mean()))
        leftColLen = int(round(left[leftCol].apply(lambda x: len(x)).mean()))

        if (rightColLen != leftColLen):
            minCol = min(rightColLen, leftColLen)
            right[rightCol] = right[rightCol].apply(lambda x: x[-minCol:])
            left[leftCol] = left[leftCol].apply(lambda x: x[-minCol:])
        t=time()

        leftGroup = left.groupby(leftCol).agg(self.myAgg)

        rightGroup = right.groupby(rightCol).agg(self.myAgg)


        leftIndexValues = left[leftCol].unique()
        rightIndexValues = right[rightCol].unique()
        print('grouping time :' , time()-t)
        t=time()

        for leftValue in leftIndexValues:
            for rightValue in rightIndexValues:
                if rightValue == leftValue:
                    join= leftGroup.merge(rightGroup, left_index=True, right_index=True, how='outer',
                                           indicator='isOuter')
                    print('legacy join time : ',time()-t)
                    return join
        return []
    def myAgg(self, col):

        if (col.name in self.left1.textCols + self.right1.textCols):
            return list(set(col))
        else:
            return sum(col)
class Result:
    def __init__(self, matcher: Matcher):
        self.matcher=matcher
        self.statistics = {'SumTotalDiff': 0, 'matchPercentOnHead': 0, 'matchPercentOnPnr': 0,
                           'exactMatchPercentOnHead': 0, 'exactMatchPercentOnSecondaryIdentifier': 0,
                           'NumberOfRowsOnly' + matcher.left1.name: 0,
                           'NumberOfRowsOnly' + matcher.right1.name: 0}
        self.resultOnHead = {'matchBoth': [], 'matchNotExact': [], 'matchExact': [],
                             matcher.left1.name + '_Only': [], matcher.right1.name + '_Only': []}
        self.resultOnSecondaryIdentifier = {'matchBoth': [], 'matchNotExact': [], 'matchExact': [],
                                            matcher.left1.name + '_Only': [], matcher.right1.name + '_Only': []}

        try:
            self.SetResultOnHead(matcher)
            print('Result Setted on Head')
        except:
            print('Result cannot setted on Head')
        try:
            self.SetResultOnSecondaryIdentifier(matcher)
            print('Result setted on Pnr')
        except:
            print('Result cannot setted on Pnr')
        try:
            if len(matcher.matchOnIdentifier)<20:
                self.statistics=pd.DataFrame()
            else:
                self.SetStatistics(matcher)
                self.statistics=pd.DataFrame(self.statistics,index=[0])
        except  :
            print('cannot reach to static ')

    def SetResultOnHead(self, matcher: Matcher):
        leftTotalCol = matcher.left1.totalCol
        rightTotalCol = matcher.right1.totalCol
        tempResultDf = matcher.matchonUniquestIdentifier.copy()
        tempResultDf['TotalDiff'] = tempResultDf[leftTotalCol] - tempResultDf[rightTotalCol]
        tempResultDf['isExact'] = tempResultDf['TotalDiff'] == 0
        self.resultOnHead['matchBoth'] = tempResultDf[tempResultDf['isOuter'] == 'both']
        self.resultOnHead['matchExact'] = self.resultOnHead['matchBoth'][
            self.resultOnHead['matchBoth']['isExact'] == True]
        self.resultOnHead['matchNotExact'] = self.resultOnHead['matchBoth'][
            self.resultOnHead['matchBoth']['isExact'] == False]
        self.resultOnHead[matcher.left1.name + '_Only'] = matcher.matchonUniquestIdentifier[
            matcher.matchonUniquestIdentifier['isOuter'] == 'left_only']
        self.resultOnHead[matcher.right1.name + '_Only'] = matcher.matchonUniquestIdentifier[
            matcher.matchonUniquestIdentifier['isOuter'] == 'right_only']


    def SetResultOnSecondaryIdentifier(self, matcher: Matcher):
        leftTotalCol = matcher.left1.totalCol
        rightTotalCol = matcher.right1.totalCol
        tempResultDf = matcher.matchOnIdentifier.copy()
        tempResultDf['TotalDiff'] = tempResultDf[leftTotalCol] - tempResultDf[rightTotalCol]
        tempResultDf['isExact'] = tempResultDf['TotalDiff'] == 0
        self.resultOnSecondaryIdentifier['matchBoth'] = tempResultDf[tempResultDf['isOuter'] == 'both']
        self.resultOnSecondaryIdentifier['matchExact'] = self.resultOnSecondaryIdentifier['matchBoth'][
            self.resultOnSecondaryIdentifier['matchBoth']['isExact'] == True]
        self.resultOnSecondaryIdentifier['matchNotExact'] = self.resultOnSecondaryIdentifier['matchBoth'][
            self.resultOnSecondaryIdentifier['matchBoth']['isExact'] == False]
        self.resultOnSecondaryIdentifier[matcher.left1.name + '_Only'] = matcher.matchOnIdentifier[
            matcher.matchOnIdentifier['isOuter'] == 'left_only']
        self.resultOnSecondaryIdentifier[matcher.right1.name + '_Only'] = matcher.matchOnIdentifier[
            matcher.matchOnIdentifier['isOuter'] == 'right_only']

    def SetStatistics(self, matcher: Matcher):
        leftTotalCol = matcher.left1.totalCol
        rightTotalCol = matcher.right1.totalCol
        tempResultDf = matcher.matchonUniquestIdentifier.copy()
        tempResultDf['TotalDiff'] = tempResultDf[leftTotalCol] - tempResultDf[rightTotalCol]
        self.statistics['SumTotalDiff'] = sum(tempResultDf['TotalDiff'].fillna(0).values)
        self.statistics['matchPercentOnHead'] = len(self.resultOnHead['matchBoth']) / len(
            matcher.matchonUniquestIdentifier)
        self.statistics['matchPercentOnPnr'] = len(self.resultOnSecondaryIdentifier['matchBoth']) / len(
            matcher.matchOnIdentifier)
        self.statistics['exactMatchPercentOnHead'] = len(self.resultOnHead['matchExact']) / len(
            self.resultOnHead['matchBoth'])
        self.statistics['exactMatchPercentOnSecondaryIdentifier'] = len(
            self.resultOnSecondaryIdentifier['matchExact']) / len(self.resultOnSecondaryIdentifier['matchBoth'])
        self.statistics['NumberOfRowsOnly' + matcher.left1.name] = len(
            matcher.matchonUniquestIdentifier[matcher.matchonUniquestIdentifier['isOuter'] == 'left_only'])
        self.statistics['NumberOfRowsOnly' + matcher.right1.name] = len(
            matcher.matchonUniquestIdentifier[matcher.matchonUniquestIdentifier['isOuter'] == 'right_only'])

    def Save(self):
        # list of dataframes
        dfsonHead = [self.statistics,self.resultOnHead['matchExact'], self.resultOnHead['matchNotExact'], self.resultOnHead[self.matcher.left1.name + '_Only'],self.resultOnHead[self.matcher.right1.name + '_Only']]
        dfsOnPnr = [self.statistics,self.resultOnSecondaryIdentifier['matchExact'], self.resultOnSecondaryIdentifier['matchNotExact']\
        , self.resultOnSecondaryIdentifier[self.matcher.left1.name + '_Only']\
        ,self.resultOnSecondaryIdentifier[self.matcher.right1.name + '_Only']]
        path='Results/_ResultsOnHead.xlsx'
        path1='Results/_ResultsOnPnr.xlsx'
        # run function
        try:
            self.multiple_dfs(dfsonHead, 'Validation',path, 5)
            print('Head file saved ')
        except:
            print('head file cannot be saved')
        try:
            self.multiple_dfs(dfsOnPnr, 'Validation1',path1, 5)
            print('Pnr File saved')
        except:
            print('Pnr File cannot be saved')

    def multiple_dfs(self,df_list, sheets, file_name, spaces):
        import xlsxwriter
        writer = pd.ExcelWriter(file_name, engine='xlsxwriter')
        row = 0

        for dataframe in df_list:
            dataframe.to_excel(writer, sheet_name=sheets, startrow=row, startcol=0)
            row = row + len(dataframe.index) + spaces + 1
        writer.save()


