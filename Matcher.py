import pandas as pd
def columnAnalyser(df):
    df = df.dropna(axis=1, how='all')
    analyzedDf = {}
    colNames = []
    uniquePercent = []
    nanPercent = []
    zeroPercent = []
    isAlfaNumeric = []
    averageLen = []
    ContainsNum = []
    standartDeviation = []
    lenDeviation=[]
    Mean = []
    for col in df.columns:
        col_len = len(df[col].tolist())
        col_sample = df[col][df[col].notnull()].sample().values[0]
        colNames.append(col)
        uniquePercent.append(len(df[col].unique().tolist()) / col_len)
        nanPercent.append(df[col].isna().sum() / col_len)
        isAlfaNumeric.append(((str(df[col][df[col].notnull()].sample().values[0])).replace(' ', "")).isalnum())
        averageLen.append(len(str((df[col][df[col].notnull()].sample().values[0])).replace(' ', "")))
        lenDeviation.append(df[col][df[col].notnull()].apply(lambda x : len(str(x))).std())

        zeroPercent.append((col_len - df[col].astype(bool).sum(axis=0)) / col_len)
        ContainsNum.append(any(str(i).isdigit() for i in str(col_sample)))
        if len(str(col_sample)) >= 10 and str(col_sample).isnumeric():
            standartDeviation.append('asStr')
            Mean.append('Nan')
        else:
            try:
                standartDeviation.append(df[col].std())

            except:
                standartDeviation.append('Nan')

            try:
                Mean.append(df[col].mean())
            except:
                Mean.append('Nan')
    analyzedDf['Column Name'] = colNames
    analyzedDf['UniquePercent'] = uniquePercent
    analyzedDf['NanPercent'] = nanPercent
    analyzedDf['zeroPercent'] = zeroPercent
    analyzedDf['isAlfaNumeric'] = isAlfaNumeric
    analyzedDf['averageLen'] = averageLen
    analyzedDf['ContainsNum'] = ContainsNum
    analyzedDf['Mean'] = Mean
    analyzedDf['lenDeviation']=lenDeviation
    analyzedDf['standartDeviation'] = standartDeviation
    return pd.DataFrame(analyzedDf) # Add Column Alphabet state
def Vektorizer(df):
    vektor = df.copy()
    vektor = vektor.drop('Column Name', axis=1)
    vektor = vektor.drop('Mean', axis=1)
    vektor = vektor.drop('standartDeviation', axis=1)
    vektor['isAlfaNumeric'] = vektor['isAlfaNumeric'].apply(lambda x: 1 if x == True else 0)
    vektor['ContainsNum'] = vektor['ContainsNum'].apply(lambda x: 1 if x == True else 0)
    vektor['averageLen'] = vektor['averageLen']

    vektor['Column'] = df['Column Name']
    # weight to col
    return vektor.query('5<averageLen<7 or 9<averageLen<14')
class Reader():
    file_index = 0

    def __init__(self, path=''):
        self.path = path
        self.file  = pd.DataFrame({'EmptyFrame': [0]})
        self.rowLen=len(self.file)
        self.colLen=len(self.file.columns)

    #helper Functions
    def FileCropper(self):
        self.rowLen = len(self.file)
        self.colLen = len(self.file.columns)
        candidateRow=[]
        indexes=list(self.file.index)
        if self.rowLen<20:
            rowIndexes=indexes
        else:
            rowIndexes=indexes[:10]+indexes[-10:]
        for row_index in rowIndexes:
            row=self.file.loc[row_index]
            nanNumberInRow=len(row[row.isna()])
            nanCastableNumberInRow=sum([1 if not self.TryCastToFloat(col) else 0 for col in row.values])
            isIncludeKeyword=any([col in ['Toplam','Total'] for col in row])
            if nanNumberInRow> self.colLen*.45 or (nanCastableNumberInRow==self.colLen) or isIncludeKeyword:
                candidateRow.append(row_index)
        for row in candidateRow:
                self.file=self.file.drop(row)
    def DateChecker(self,date):
        from dateutil import parser
        try:
            a = parser.parse(date).date()
            return True
        except:
            return False
    def DateDropper(self):
        import  re
        import numpy
        dateRegexList = ['^([1-9]|([012][0-9])|(3[01]))-([0]{0,1}[1-9]|1[012])-\d\d\d\d [012]{0,1}[0-9]:[0-6][0-9]$', \
                         '20\d{2}(-|\/)((0[1-9])|(1[0-2]))(-|\/)((0[1-9])|([1-2][0-9])|(3[0-1]))(\s)(([0-1][0-9])|(2[0-3])):([0-5][0-9]):([0-5][0-9])', \
                         '^([1-9]|([012][0-9])|(3[01]))-([0]{0,1}[1-9]|1[012])-\d\d\d\d (20|21|22|23|[0-1]?\d):[0-5]?\d:[0-5]?\d$', \
                         '/(?:(\d{4})([\-\/.])([0-3]?\d)\2([0-3]?\d)|([0-3]?\d)([\-\/.])([0-3]?\d)\6(\d{4}))(?:\s+([012]?\d)([:hap])([0-5]\d))?/', \
                         '\d{4}-\d{2}-\d{2}\s+\d{2}:\d{2}:\d{2}.0*', \
                         "/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]) (0[0-9]|1[0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/"]
        for col in self.file.columns:
            try:
                dateCandidate = self.file[col][self.file[col].notnull()].sample().values[0]
            except:
                dateCandidate = ''

            if type(dateCandidate) == 'datetime.datetime' or type(dateCandidate) == numpy.datetime64 or self.DateChecker(
                    dateCandidate):
                self.file = self.file.drop([col], axis=1)
            else:
                for dateR in dateRegexList:
                    if re.match(dateR, str(dateCandidate)):
                        self.file = self.file.drop([col], axis=1)
    def TryCastToFloat(self,x):
        import locale
        import numpy as np
        try:

            locale.setlocale(locale.LC_ALL, 'Turkish_Turkey.1254')
        except locale.Error as e:
            locale.setlocale(locale.LC_ALL, 'C')
            print('localization failed,export your LC_ALL TO Turkish_Turkey.1254', e)
        try:
            float(x)
            return True
        except:
            if (type(x) is str) and (x.find('.') != -1 or x.find('.T') != -1):
                x = x.replace('.T', '.0')
                x = x.replace('.', ',')
            elif type(x) in (int, float, np.float, np.int):
                return True
            try:
                locale.atof(x)
                return True
            except:
                try:
                    x = x.replace(',', '.', 1)
                    locale.atof(x)
                    return True
                except:
                    return False
    #end helper Functions
    def Read(self, path):
        # to number of file
        Reader.file_index += 1
        file_sample = 'file' + str(Reader.file_index)
        try:
            self.file = pd.read_csv(path, sep='\s*\*?\,?', engine='python', names='')
            self.file.columns = [str(file_sample + "_Col" + str(i)) for i in range(len(self.file.columns))]

            print("File're reading with regex='\s*\*'...")
            print('Succeed,Done')
        except pd.errors.ParserError as e:
            print(e)
            print("File Cannot read,other types're being tried..")
            try:
                self.file = pd.read_excel(path)
                self.file.columns = [file_sample + str("_Col" + str(i)) for i in range(len(self.file.columns))]

                print('Succeed,Done')

            except:
                print('file cannot read')

        self.FileCropper()
        self.DateDropper()
        self.file = self.file.dropna(axis=1, thresh=int(len(self.file)*0.8), how='all')
        self.file = self.file.dropna(axis=0, thresh=int(len(self.file.columns)*0.8), how='all')
        return self.file
class Matcher():
    matchPercent=0
    def __init__(self,left,right):
        self.left=left
        self.right=right
        self.analayzedLeft=columnAnalyser(left)
        self.analayzedRight=columnAnalyser(right)
        self.leftVector=Vektorizer(self.analayzedLeft)
        self.leftVector['priority'] = self.leftVector['UniquePercent'] * 5 + (5 * (1 - self.leftVector['lenDeviation'])) + (1 - self.leftVector['zeroPercent'])
        self.leftVectorCandidates=self.leftVector.sort_values('priority',ascending=False)[:3]
        self.rightVector=Vektorizer(self.analayzedRight)
        self.rightVector['priority'] = self.rightVector['UniquePercent'] * 5 + (5 * (1 - self.rightVector['lenDeviation'])) + (1 - self.rightVector['zeroPercent'])
        self.rightVectorCandidates = self.rightVector.sort_values('priority', ascending=False)[:3]
        self.pairCandidates=[]
        self.matchCandidate=[]
        self.leftOut=[]
        self.rightOut=[]
        self.inner=[]

    def fitAndEstimate(self):
        from scipy.spatial import distance
        pairs = {'leftCol': [], 'rightCol': [], 'distance': [],'uniquer':[]}
        leftCols = []
        rightCols = []
        distances = []
        uniquer=[]
        for (leftColName, leftVector) in zip(self.leftVectorCandidates['Column'].values, self.leftVectorCandidates.iloc[:,:-2].values):
            for (rightColName, rightVector) in zip(self.rightVectorCandidates['Column'].values,
                                                   self.leftVectorCandidates.iloc[:, :-2].values):
                leftCols.append(leftColName)
                rightCols.append(rightColName)
                distances.append(distance.euclidean(leftVector, rightVector))
                uniquer.append(leftVector[0]+rightVector[0])
        pairs['leftCol'] = leftCols
        pairs['rightCol'] = rightCols
        pairs['distance'] = distances
        pairs['uniquer']=uniquer
        df=pd.DataFrame(pairs)
        self.pairCandidates= df.sort_values('distance').iloc[:,:2].values

        for leftCol,rightCol in self.pairCandidates:
            self.matchCandidate.append(self.RowMatch(self.left,self.right,leftCol,rightCol))
        return pd.DataFrame(self.matchCandidate).dropna(axis=0)[0]



    def RowMatch(self,left, right, leftCol, rightCol):
        try:
            left[leftCol] = left[leftCol].apply(lambda x: str(int(x)))
            right[rightCol] = right[rightCol].apply(lambda x: str(int(x)))
        except:
            left[leftCol] = left[leftCol].apply(lambda x: str(x))
            right[rightCol] = right[rightCol].apply(lambda x: str(x))

        rightColLen = int(round(right[rightCol].apply(lambda x: len(x)).mean()))
        leftColLen = int(round(left[leftCol].apply(lambda x: len(x)).mean()))
        print(rightColLen, leftColLen)

        if (rightColLen != leftColLen):
            minCol = min(rightColLen, leftColLen)
            right[rightCol] = right[rightCol].apply(lambda x: x[-minCol:])
            left[leftCol] = left[leftCol].apply(lambda x: x[-minCol:])

        leftGroup = left.groupby(leftCol).agg(lambda x: list(x))
        rightGroup = right.groupby(rightCol).agg(lambda x: list(x))
        outerMerge= leftGroup.merge(rightGroup, left_index=True, right_index=True, how='outer',
                                          indicator='isOuter')
        if len(outerMerge[outerMerge['isOuter']=='both'])==0:
            return None
        else :
            return outerMerge


