import pandas as pd
import numpy as np
import warnings

from File import File, Matcher, Result
from Template import TemplateList,Templater,Template
from Action import Action,ActionList,ActionListLoader
from sklearn.linear_model import LogisticRegression,LinearRegression
from time import time


#UPDATE THIS METHOD
def templatePairer(preTemplate:Template, candidate:Template):
        preCols=set(list(preTemplate.colNames))
        canCols=set(list(candidate.colNames))
        if preCols.issubset(canCols) or preCols.issuperset(canCols) or len(preCols.intersection(canCols))>len(preCols.difference(canCols).union(canCols.difference(preCols))):
            return True


def templateBuilder(file:File,template:Template):

    file.build()
    identifier=file.identifier
    uniquestIdentifier=file.uniquestIdentifier
    totalCol=file.totalCol
    dependedCol=None
    behavior = Template.Behavior(uniquestIdentifier,identifier,totalCol)
    template.behavior=behavior
    template.behavior.action=None
    template.behavior.textCols=file.textCols
    template.behavior.numericCols=file.numericCols
    template.behavior.leanFrame=file.leanFrame
    template.behavior.name=file.name
    template.behavior.analyzed=file.analayzedForm

    return template


def Logical_Coef_Decider(logicalResult,action):
    import matplotlib.pyplot as plt

    Y=logicalResult['isExact'].apply(lambda  x: not x )
    X=logicalResult.drop(['isExact'],axis=1)
    X = X.apply(lambda x: x.apply(lambda y: str(y)))
    if len(X.columns)>0:
        X=pd.get_dummies(X)
        X['isExact']=Y
        correlationThreshold=0.8
        negative_correlation_paramaters=pd.DataFrame(X.corr()['isExact'].drop(['isExact'],axis=0)).query('isExact>'+str(correlationThreshold)+'').index
        parsed_index=[item.split('_') for item in negative_correlation_paramaters]
        for splittedItem in parsed_index:
            name=splittedItem[0]
            columnName=splittedItem[0]+'_'+splittedItem[1]
            colVal=splittedItem[2]
            if action.left.behavior.name==name:
                action.left.behavior.logicalDependencies['columnName']=columnName
                action.left.behavior.logicalDependencies['colVal'] = colVal
            elif action.right.behavior.name==name:
                action.right.behavior.logicalDependencies['columnName']=columnName
                action.right.behavior.logicalDependencies['colVal'] = colVal
            else:
                print('there is no LogicalDependencise')
    return  action
def Linear_Coef_Decider(LinearResult,behavior:Template.Behavior):
    epsilon =0.2
    A=behavior.leanFrame[behavior.numericCols].drop(behavior.totalCol,axis=1)
    B=behavior.leanFrame[behavior.totalCol]

    coef_ = {}
    while len(A.columns)>0:
        res = {}
        x, _, _, _ = np.linalg.lstsq(A, B)

        for key, val in zip(A.columns, x):
            res[key] = val
        for key,val,i in zip(res.keys(),res.values(),range(len(x))):
            if (val !=0 and  1-epsilon<val<1+epsilon):
                B=B-A[key]
                A=A.drop(key,axis=1)
                coef_[key]=1
            elif val==0:
                A=A.drop(key,axis=1)
                coef_[key]=0
            else:
                totalMean=behavior.analyzed[behavior.analyzed['Column Name']==behavior.totalCol]['Mean'].values[0]
                keyMean=behavior.analyzed[behavior.analyzed['Column Name']==key]['Mean'].values[0]
                if  totalMean<keyMean and abs(val)<epsilon:
                    A = A.drop(key, axis=1)
                    coef_[key] = 0
                else:
                    minKey = min(res, key=res.get)
                    coef_[minKey] = 0
                    A=A.drop([minKey],axis=1)
                    break
            break

    return coef_
def postMatcher(action:Action):

    matcher=Matcher(action.left,action.right)
    t=time()

    matcher.fitAndEstimate()
    print('match and join take :',time()-t)

    result=Result(matcher)
    result.Save()
    #get Confirm to Serialize
    action.serialize()




def Actioner(left,right,result):
    action=Action(left,right)
    #preparing Lineer intended Result set

    trimmedColLeft = left.behavior.analyzed
    trimmedColRight = right.behavior.analyzed
    trimmedColLeft = trimmedColLeft[trimmedColLeft['Column Name'].isin(left.behavior.numericCols)].query('Mean!=0')
    trimmedColRight = trimmedColRight[trimmedColRight['Column Name'].isin(right.behavior.numericCols)].query('Mean!=0')

    # Detect lineer Dependencies
    try:
        lineerResult=result[list(trimmedColLeft['Column Name'])+list(trimmedColRight['Column Name'])+['TotalDiff']]
        right.behavior.lineerDependencies=Linear_Coef_Decider(lineerResult,action.right.behavior)
        left.behavior.lineerDependencies=Linear_Coef_Decider(lineerResult,action.left.behavior)
    except:
        print('cant lineer')
    #preparing Logical intended Result set
    trimmedColLeft=left.behavior.analyzed
    trimmedColRight=right.behavior.analyzed
    trimmedColLeft=trimmedColLeft[trimmedColLeft['Column Name'].isin(left.behavior.textCols)].query('UniquePercent<0.1 and 2<averageLen<6' )
    trimmedColRight=trimmedColRight[trimmedColRight['Column Name'].isin(right.behavior.textCols)].query('UniquePercent<0.1 and 2<averageLen<6' )
    # Detect Logical Dependencies with respect to resul set
    logicalResult=result[list(trimmedColLeft['Column Name'])+list(trimmedColRight['Column Name'])+['isExact']]
    action=Logical_Coef_Decider(logicalResult,action)


    return action
def actionBuilder(left:Template,right:Template):
    matcher=Matcher(left,right)
    matcher.fitAndEstimate()
    result=Result(matcher).resultOnHead['matchBoth']
    action= Actioner(left,right,result)
    return  action
def actionPairer(left:Template, right:Template, candidateAction:Action):
    if (templatePairer(left,candidateAction.left)and templatePairer(right,candidateAction.right)):
        return candidateAction
    elif (templatePairer(right,candidateAction.left)and templatePairer(left,candidateAction.right)):
        action=Action(left,right)#to swap their place
        action.left=candidateAction.right
        action.right=candidateAction.left
        action.actionName=candidateAction.actionName
        action.serialized=candidateAction.serialized

        return action
    else:
        return None
def searchAction(left:Template, right:Template):
    actionList=ActionList()
    actionList.Load()
    aList=actionList.actionList
    for candidateAction in aList:
        if actionPairer(left,right,candidateAction):
            return candidateAction
    return None



def MainBuilder(leftPath:str,rightPath:str):
    t=time()
    left=File(leftPath)
    left.read()
    right=File(rightPath)
    right.read()
    print('file reading take :',time()-t)
    t=time()

    templater=Templater()
    rightTemplate=templater.preBuildTemplate(left.base,left.originalColumn,left.sample)
    leftTemplate=templater.preBuildTemplate(right.base,right.originalColumn,right.sample)
    action=searchAction(leftTemplate,rightTemplate)
    print('Searcing Action, and build pre temlate take :',time()-t)

    if action is None:
        t = time()
        print("No proper action in file ,Action building...")
        leftTemplate=templateBuilder(left,leftTemplate)
        rightTemplate=templateBuilder(right,rightTemplate)
        action=actionBuilder(leftTemplate,rightTemplate)
        print('analyz and build action take :', time() - t)

    else:
        action.left.behavior.leanFrame=left.leanFrame
        action.right.behavior.leanFrame = right.leanFrame
        print("proper action Action Selected.")

    matcher=postMatcher(action)


if __name__ == '__main__':
    warnings.simplefilter(action='ignore', category=FutureWarning)

    t=time()
    biletbank='reports/allin/TK_MERKEZ_01.05.2019__714/bilet_merkez_tk.xlsx'
    provider='reports/allin/TK_MERKEZ_01.05.2019__714/tk_merkez.xlsx'
    MainBuilder(biletbank,provider)
    print("execution time:",time()-t)
    #atlas= File("")
    #atlasTemplate=templateBuilder(atlas)

    #bilet = File("reports/allin/ATLASG_WEB_29.04.2019__504/bilet.xlsx")
    #biletTemplate=templateBuilder(bilet)



